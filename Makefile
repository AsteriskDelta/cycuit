NAME = Cycuit

#Directory that contains .cpp files with main()
EXEC_DIRS = ./specs/

#directories with classes/files to be compiled into objects
SRC_DIRS =

#directories containing headers that need to be installed
HEADER_DIRS = layout sim render
HEADER_CP_DIRS =
HEADER_FILES += Cycuit.h CycuitInclude.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = Cycuit/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
PCH =

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS += `freetype-config --cflags`
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?=
#Optimization flags, supporting PGO if needed
OPTI ?= -march=native -mtune=native -Ofast -flto
#Include paths, ie -I/path/to/headers/
INCPATH +=
#Libraries, ie -lopenmp
LIBS +=
#Warnings, ie -Wall
WARNINGS ?= -Wall -Wextra -Wnon-virtual-dtor -Wcast-align -Wshadow -Wno-unused-result

include /usr/include/LPBT.mk

install::

uninstall::

autorun::

disable::
