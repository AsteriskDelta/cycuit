#ifndef CYCUIT_ATTRIBSET_H
#define CYCUIT_ATTRIBSET_H
#include "CycuitInclude.h"
#include "Attribute.h"

namespace Cycuit {
    class AttributeSet : public nvx::genv_t {
    public:
        typedef nvx::genv_t Base;
        AttributeSet();
        AttributeSet(const Base& base);
        ~AttributeSet();

        inline auto& impedence() {
            return this->operator[](0);
        }
        inline auto& capacitance() {
            return this->operator[](1);
        }
        inline auto& inductance() {
            return this->operator[](2);
        }

        inline const auto& impedence() const {
            return this->operator[](0);
        }
        inline const auto& capacitance() const {
            return this->operator[](1);
        }
        inline const auto& inductance() const {
            return this->operator[](2);
        }
    };
}

#endif
