#ifndef CYCUIT_PHYECTOR_H
#define CYCUIT_PHYECTOR_H
#include "CycuitInclude.h"

namespace Cycuit {
    class Phyector : public nvx::genv_t {
    public:
        typedef nvx::genv_t Base;
        Phyector();
        Phyector(const Base& base);
        ~Phyector();

        inline auto& frequency() {
            return this->operator[](0);
        }
        inline auto& amplitude() {
            return this->operator[](1);
        }
        inline auto& phase() {
            return this->operator[](2);
        }

        inline const auto& frequency() const {
            return this->operator[](0);
        }
        inline const auto& amplitude() const {
            return this->operator[](1);
        }
        inline const auto& phase() const {
            return this->operator[](2);
        }
    };
}

#endif
