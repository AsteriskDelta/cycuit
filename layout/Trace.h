#ifndef CYCUIT_TRACE_H
#define CYCUIT_TRACE_H

namespace Cycuit {
    class Trace : public Subpart {
    public:
        Trace();
        virtual ~Trace();

        nvx::path2d_t path;
    protected:

    };
}

#endif
