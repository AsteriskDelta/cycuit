#ifndef CYCUIT_SUBPART_H
#define CYCUIT_SUBPART_H
#include "CycuitInclude.h"
#include "Drawable.h"
#include <ARKE/Smallmap.h>

namespace Cycuit {
    class Part;

    class Subpart {
    public:
        Subpart();
        virtual ~Subpart();

        inline virtual bool trivial() const {
            return true;
        }
        inline virtual Part* part() {
            return nullptr;
        }

        Smallmap<unsigned int, Subpart*> idealConnections, connections;

        std::list<Drawable*> drawables();
    protected:
        Subpart *prototype;
        struct raw {
            std::list<Drawable*> drawables;
        };
    };
}

#endif
